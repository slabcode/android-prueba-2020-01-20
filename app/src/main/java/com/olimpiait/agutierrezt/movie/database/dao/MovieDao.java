package com.olimpiait.agutierrezt.movie.database.dao;

import com.olimpiait.agutierrezt.movie.database.model.movies.MovieResult;
import com.olimpiait.agutierrezt.movie.database.model.movies.MovieWithGenre;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

@Dao
public interface MovieDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void save(MovieResult movie);

    @Query("SELECT * from movies WHERE page = :page")
    LiveData<List<MovieResult>> getAllMovies(int page);

    @Transaction
    @Query("SELECT * from movies WHERE movieId = :id LIMIT 1")
    LiveData<MovieWithGenre> getMovie(int id);

    @Query("SELECT * from movies WHERE page = :page LIMIT 1")
    MovieResult hasMovies(int page);

}
