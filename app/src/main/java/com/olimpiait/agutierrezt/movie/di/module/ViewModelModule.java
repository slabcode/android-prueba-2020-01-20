package com.olimpiait.agutierrezt.movie.di.module;

import com.olimpiait.agutierrezt.movie.di.key.ViewModelKey;
import com.olimpiait.agutierrezt.movie.repository.GenreRepository;
import com.olimpiait.agutierrezt.movie.repository.MovieRepository;
import com.olimpiait.agutierrezt.movie.repository.TvRepository;
import com.olimpiait.agutierrezt.movie.viewmodel.FactoryViewModel;
import com.olimpiait.agutierrezt.movie.viewmodel.GenreViewModel;
import com.olimpiait.agutierrezt.movie.viewmodel.MovieViewModel;
import com.olimpiait.agutierrezt.movie.viewmodel.TvShowViewModel;

import java.util.Map;

import javax.inject.Provider;

import androidx.lifecycle.ViewModel;
import dagger.Module;
import dagger.Provides;
import dagger.multibindings.IntoMap;

@Module
public class ViewModelModule {

    @Provides
    FactoryViewModel viewModelFactory(Map<Class<? extends ViewModel>, Provider<ViewModel>> providerMap) {
        return new FactoryViewModel(providerMap);
    }

    @Provides
    @IntoMap
    @ViewModelKey(GenreViewModel.class)
    ViewModel genreViewModel(GenreRepository genreRepository) {
        return new GenreViewModel(genreRepository);
    }

    @Provides
    @IntoMap
    @ViewModelKey(MovieViewModel.class)
    ViewModel movieViewModel(MovieRepository movieRepository) {
        return new MovieViewModel(movieRepository);
    }

    @Provides
    @IntoMap
    @ViewModelKey(TvShowViewModel.class)
    ViewModel tvShowViewModel(TvRepository tvRepository) {
        return new TvShowViewModel(tvRepository);
    }
}
