package com.olimpiait.agutierrezt.movie.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.olimpiait.agutierrezt.movie.R;
import com.olimpiait.agutierrezt.movie.database.model.movies.MovieResult;
import com.olimpiait.agutierrezt.movie.databinding.MovieBinding;
import com.olimpiait.agutierrezt.movie.interfaces.MovieClickListener;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.ViewHolder>  {
    private MovieClickListener movieClickListener;
    private List<MovieResult> movies;
    private Context context;
    private LayoutInflater layoutInflater;

    public MovieAdapter(List<MovieResult> movies, Context context) {
        this.movies = movies;
        this.context = context;
    }

    public void setMovieClickListener(MovieClickListener movieClickListener) {
        this.movieClickListener = movieClickListener;
    }

    @Override
    public void onBindViewHolder(@NonNull MovieAdapter.ViewHolder holder, int position) {
        MovieResult movieResult = movies.get(position);
        holder.movieBinding.getRoot().findViewById(R.id.movie_recycler_card).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                movieClickListener.setOnClickListener(movieResult.getId());
            }
        });
        holder.bind(movieResult);
    }

    @NonNull
    @Override
    public MovieAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (layoutInflater==null){
            layoutInflater=LayoutInflater.from(parent.getContext());
        }
        MovieBinding movieBinding= DataBindingUtil.inflate(layoutInflater, R.layout.movie_item,parent,false);
        return new MovieAdapter.ViewHolder(movieBinding);
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        private MovieBinding movieBinding;
        public ViewHolder(@NonNull MovieBinding movieBinding) {
            super(movieBinding.getRoot());
            this.movieBinding=movieBinding;
        }
        public void bind(MovieResult movieResult){
            this.movieBinding.setMovie(movieResult);
            movieBinding.executePendingBindings();
        }
        public MovieBinding getMovieBinding(){
            return movieBinding;
        }
    }

}
