package com.olimpiait.agutierrezt.movie.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import dagger.android.AndroidInjection;

import android.os.Bundle;

import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.olimpiait.agutierrezt.movie.R;
import com.olimpiait.agutierrezt.movie.databinding.MovieWithGenreBinding;
import com.olimpiait.agutierrezt.movie.databinding.TvWithGenreBinding;
import com.olimpiait.agutierrezt.movie.util.Constants;
import com.olimpiait.agutierrezt.movie.viewmodel.FactoryViewModel;
import com.olimpiait.agutierrezt.movie.viewmodel.MovieViewModel;
import com.olimpiait.agutierrezt.movie.viewmodel.TvShowViewModel;

import javax.inject.Inject;

public class TvDetailActivity extends AppCompatActivity {

    private TvWithGenreBinding tvWithGenreBinding;
    @Inject
    FactoryViewModel factoryViewModel;
    private TvShowViewModel tvShowViewModel;
    private int id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        configureDagger();
        tvWithGenreBinding = DataBindingUtil.setContentView(this,R.layout.activity_tv_detail);
        id = getIntent().getIntExtra(Constants.ID,0);
        init();
    }

    private void init() {
        tvShowViewModel = ViewModelProviders.of(this, factoryViewModel).get(TvShowViewModel.class);
        tvShowViewModel.getTvShowById(id).observe(this, tvWithGenre -> {
            tvWithGenreBinding.setTvWithGenre(tvWithGenre);
        });

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        CollapsingToolbarLayout collapsingToolbarLayout =
                (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);

    }

    private void configureDagger(){
        AndroidInjection.inject(this);
    }
}
