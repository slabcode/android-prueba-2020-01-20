package com.olimpiait.agutierrezt.movie.database.converter;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import androidx.room.TypeConverter;

public class DateConverters {
    @TypeConverter
    public static Date fromString(String value) {
        if(value == null)
            return null;

        try {
            Date date =new SimpleDateFormat("yyyy-MM-dd").parse(value);
            return date;
        } catch (ParseException e) {
            return null;
        }

    }

    @TypeConverter
    public static String dateToString(Date date) {
        if(date == null)
            return null;
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String strDate = dateFormat.format(date);
        return strDate;
    }
}
