package com.olimpiait.agutierrezt.movie.database.model.movies;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Movie {
    @SerializedName("page")
    @Expose
    private int page;
    @SerializedName("total_pages")
    @Expose
    private int totalPages;
    @SerializedName("results")
    private List<MovieResult> results;

    public Movie(int page, int totalPages, List<MovieResult> results) {
        this.page = page;
        this.totalPages = totalPages;
        this.results = results;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public List<MovieResult> getResults() {
        return results;
    }

    public void setResults(List<MovieResult> results) {
        this.results = results;
    }
}
