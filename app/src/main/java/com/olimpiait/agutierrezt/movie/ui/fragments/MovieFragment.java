package com.olimpiait.agutierrezt.movie.ui.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.olimpiait.agutierrezt.movie.R;
import com.olimpiait.agutierrezt.movie.database.model.movies.MovieResult;
import com.olimpiait.agutierrezt.movie.interfaces.ChangeActivityListener;
import com.olimpiait.agutierrezt.movie.interfaces.MovieClickListener;
import com.olimpiait.agutierrezt.movie.ui.adapters.GenreAdapter;
import com.olimpiait.agutierrezt.movie.ui.adapters.MovieAdapter;
import com.olimpiait.agutierrezt.movie.util.Constants;
import com.olimpiait.agutierrezt.movie.util.EndlessRecyclerViewScrollListener;
import com.olimpiait.agutierrezt.movie.viewmodel.FactoryViewModel;
import com.olimpiait.agutierrezt.movie.viewmodel.GenreViewModel;
import com.olimpiait.agutierrezt.movie.viewmodel.MovieViewModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import dagger.android.support.AndroidSupportInjection;

public class MovieFragment extends Fragment {
    private RecyclerView recyclerView;
    private MovieAdapter movieAdapter;
    private LinearLayoutManager mLayoutManager;
    @Inject
    FactoryViewModel factoryViewModel;
    private MovieViewModel movieViewModel;
    private boolean hasFinish = false;
    private List<MovieResult> movies;
    private ChangeActivityListener changeActivityListener;

    public void setChangeActivityListener(ChangeActivityListener changeActivityListener) {
        this.changeActivityListener = changeActivityListener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_movie, container, false);
        configureDagger();
        init(view);
        events();
        return view;
    }

    private void init(View view) {
        movies = new ArrayList<>();
        recyclerView = view.findViewById(R.id.movie_recycler);
        mLayoutManager = new LinearLayoutManager(MovieFragment.this.getActivity());
        movieAdapter = new MovieAdapter(movies,MovieFragment.this.getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL));
        recyclerView.setAdapter(movieAdapter);
        movieViewModel = ViewModelProviders.of(this, factoryViewModel).get(MovieViewModel.class);
        loadMovies(1);

    }

    private void events() {
        recyclerView.setOnScrollListener(new EndlessRecyclerViewScrollListener(mLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if(!hasFinish) {
                    loadMovies(page + 1);
                }
            }
        });
        movieAdapter.setMovieClickListener(new MovieClickListener() {
            @Override
            public void setOnClickListener(int id) {
                changeActivityListener.setOnClickListener(id, Constants.MOVIE);
            }
        });
    }

    private void loadMovies(int page) {
        movieViewModel.getMovies(page).observe(this,movieResults -> {
            if(movieResults.size() > 0) {
                movies.addAll(movieResults);
                movieAdapter.notifyDataSetChanged();
            }else {
                hasFinish = true;
            }
        });
    }

    private void configureDagger(){
        AndroidSupportInjection.inject(this);
    }
}
