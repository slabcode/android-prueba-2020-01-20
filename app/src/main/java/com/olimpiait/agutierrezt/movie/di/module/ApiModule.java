package com.olimpiait.agutierrezt.movie.di.module;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.olimpiait.agutierrezt.movie.api.MovieApi;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module(includes = OkHttpClientModule.class)
public class ApiModule {
    private final String BASE_URL = "https://api.themoviedb.org/3/";
    @Provides
    public MovieApi movieApi(Retrofit retrofit) {
        return retrofit.create(MovieApi.class);
    }

    @Provides
    public Retrofit retrofit(OkHttpClient okHttpClient, GsonConverterFactory gsonConverterFactory) {
        return new Retrofit.Builder()
                    .client(okHttpClient)
                    .baseUrl(BASE_URL)
                    .addConverterFactory(gsonConverterFactory)
                    .build();
    }

    @Provides
    public Gson gson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        return gsonBuilder.create();
    }

    @Provides
    public GsonConverterFactory gsonConverterFactory(Gson gson) {
        return  GsonConverterFactory.create(gson);
    }
}
