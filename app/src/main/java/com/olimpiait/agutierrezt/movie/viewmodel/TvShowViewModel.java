package com.olimpiait.agutierrezt.movie.viewmodel;

import com.olimpiait.agutierrezt.movie.database.model.tv_shows.TvResult;
import com.olimpiait.agutierrezt.movie.database.model.tv_shows.TvWithGenre;
import com.olimpiait.agutierrezt.movie.repository.TvRepository;

import java.util.List;

import javax.inject.Inject;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

public class TvShowViewModel extends ViewModel {
    private TvRepository tvRepository;

    @Inject
    public TvShowViewModel(TvRepository tvRepository) {
        this.tvRepository = tvRepository;
    }

    public LiveData<List<TvResult>> getTvShows(int page) {
        return tvRepository.getAllTvShows(page);
    }

    public LiveData<TvWithGenre> getTvShowById(int id) {
        return tvRepository.getTvShowById(id);
    }
}
