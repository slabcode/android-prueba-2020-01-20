package com.olimpiait.agutierrezt.movie.di.module;

import com.olimpiait.agutierrezt.movie.ui.fragments.GenreFragment;
import com.olimpiait.agutierrezt.movie.ui.fragments.MovieFragment;
import com.olimpiait.agutierrezt.movie.ui.fragments.TvFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class FragmentModule {
    @ContributesAndroidInjector
    abstract GenreFragment contributeGenreFragment();
    @ContributesAndroidInjector
    abstract MovieFragment contributeMovieragment();
    @ContributesAndroidInjector
    abstract TvFragment contributeTvragment();
}
