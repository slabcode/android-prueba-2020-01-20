package com.olimpiait.agutierrezt.movie.database.dao;

import com.olimpiait.agutierrezt.movie.database.model.genres.GenreResult;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

@Dao
public interface GenreDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void save(List<GenreResult> genres);

    @Query("SELECT * FROM genres")
    LiveData<List<GenreResult>> getAllGenres();

    @Query("SELECT * FROM genres LIMIT 1")
    GenreResult hasGenres();
}
