package com.olimpiait.agutierrezt.movie.util;

public class Constants {
    public static final String GENRE = "GENRE";
    public static final String MOVIE = "MOVIE";
    public static final String TV = "TV";
    public static final String ID = "ID";

}
