package com.olimpiait.agutierrezt.movie.viewmodel;

import com.olimpiait.agutierrezt.movie.database.model.genres.GenreResult;
import com.olimpiait.agutierrezt.movie.repository.GenreRepository;

import java.util.List;

import javax.inject.Inject;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

public class GenreViewModel extends ViewModel {
    private GenreRepository genreRepository;

    @Inject
    public GenreViewModel(GenreRepository genreRepository) {
        this.genreRepository = genreRepository;
    }

    public LiveData<List<GenreResult>> getAllGenres() {
        return genreRepository.getAllGenres();
    }
}
