package com.olimpiait.agutierrezt.movie.database.dao;

import com.olimpiait.agutierrezt.movie.database.model.tv_shows.TvResult;
import com.olimpiait.agutierrezt.movie.database.model.tv_shows.TvWithGenre;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

@Dao
public interface TvDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void save(TvResult movie);

    @Query("SELECT * from tv_shows WHERE page = :page")
    LiveData<List<TvResult>> getAllTvShows(int page);

    @Transaction
    @Query("SELECT * from tv_shows WHERE tvId = :id LIMIT 1")
    LiveData<TvWithGenre> getTvShowById(int id);

    @Query("SELECT * from tv_shows WHERE page = :page LIMIT 1")
    TvResult hasTvShows(int page);
}
