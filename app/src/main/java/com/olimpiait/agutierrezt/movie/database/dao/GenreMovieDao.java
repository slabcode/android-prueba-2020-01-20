package com.olimpiait.agutierrezt.movie.database.dao;

import com.olimpiait.agutierrezt.movie.database.model.genres.GenreMovie;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

@Dao
public interface GenreMovieDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void save(GenreMovie genreMovie);

    @Query("SELECT * FROM genre_movies WHERE movieId = :movieId AND genreId = :genreId  LIMIT 1")
    LiveData<GenreMovie> hasGenreMovie(int movieId, int genreId);
}
