package com.olimpiait.agutierrezt.movie.database.model.movies;

import android.util.Log;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.olimpiait.agutierrezt.movie.database.converter.DateConverters;

import java.util.Date;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.databinding.BindingAdapter;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "movies")
public class MovieResult {
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "movieId")
    @SerializedName("id")
    @Expose
    private int id;
    @NonNull
    @ColumnInfo(name = "votes")
    @SerializedName("vote_count")
    @Expose
    private int votes;
    @NonNull
    @ColumnInfo(name = "poster")
    @SerializedName("poster_path")
    @Expose
    private String poster;
    @NonNull
    @ColumnInfo(name = "title")
    @SerializedName("title")
    @Expose
    private String title;
    @NonNull
    @ColumnInfo(name = "vote_average")
    @SerializedName("vote_average")
    @Expose
    private float voteAverage;
    @NonNull
    @ColumnInfo(name = "overview")
    @SerializedName("overview")
    @Expose
    private String overview;
    @NonNull
    @ColumnInfo(name = "release_date")
    @SerializedName("release_date")
    @Expose
    private Date release;
    @Ignore
    @SerializedName("genre_ids")
    @Expose
    private List<Integer> genres;
    @NonNull
    @ColumnInfo(name = "page")
    private int page;
    @NonNull
    @ColumnInfo(name = "total_pages")
    private int totalPages;

    public MovieResult() {
    }

    public MovieResult(@NonNull int id, int votes, String poster, String title, float voteAverage, String overview, Date release, List<Integer> genres) {
        this.id = id;
        this.votes = votes;
        this.poster = poster;
        this.title = title;
        this.voteAverage = voteAverage;
        this.overview = overview;
        this.release = release;
        this.genres = genres;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getVotes() {
        return votes;
    }

    public void setVotes(int votes) {
        this.votes = votes;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public float getVoteAverage() {
        return voteAverage;
    }

    public void setVoteAverage(float voteAverage) {
        this.voteAverage = voteAverage;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public Date getRelease() {
        return release;
    }

    public void setRelease(Date release) {
        this.release = release;
    }

    public List<Integer> getGenres() {
        return genres;
    }

    public void setGenres(List<Integer> genres) {
        this.genres = genres;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    @BindingAdapter({"imageUrl"})
    public static void loadimage(ImageView imageView, String imageUrl){
        RequestOptions options = new RequestOptions();
        options.centerCrop();
        Glide.with(imageView.getContext()).load("https://image.tmdb.org/t/p/w500" + imageUrl)
                .apply(options)
                .into(imageView);
    }

    public String shorterText() {
        if(overview.length() < 50)
            return overview;
        return overview.substring(0,50) + "...";
    }

    public String voteAverageText() {
        return voteAverage + "";
    }

    public String dateStr() {
        return DateConverters.dateToString(release);
    }

}
