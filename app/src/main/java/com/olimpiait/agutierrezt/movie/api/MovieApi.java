package com.olimpiait.agutierrezt.movie.api;

import com.olimpiait.agutierrezt.movie.database.model.genres.Genre;
import com.olimpiait.agutierrezt.movie.database.model.movies.Movie;
import com.olimpiait.agutierrezt.movie.database.model.tv_shows.Tv;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface MovieApi {
    @GET("movie/top_rated")
    Call<Movie> GetMovieTopRated(@Query("page") int page, @Query("api_key") String apiKey);

    @GET("genre/list")
    Call<Genre> GetGenres(@Query("api_key") String apiKey);

    @GET("tv/top_rated")
    Call<Tv> GetTvTopRated(@Query("page") int page, @Query("api_key") String apiKey);


}
