package com.olimpiait.agutierrezt.movie.interfaces;

public interface ChangeActivityListener {
    void setOnClickListener(int id, String type);
}
