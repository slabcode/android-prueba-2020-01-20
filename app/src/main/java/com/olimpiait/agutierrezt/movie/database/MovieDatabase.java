package com.olimpiait.agutierrezt.movie.database;

import com.olimpiait.agutierrezt.movie.database.converter.DateConverters;
import com.olimpiait.agutierrezt.movie.database.dao.GenreDao;
import com.olimpiait.agutierrezt.movie.database.dao.GenreMovieDao;
import com.olimpiait.agutierrezt.movie.database.dao.GenreTvDao;
import com.olimpiait.agutierrezt.movie.database.dao.MovieDao;
import com.olimpiait.agutierrezt.movie.database.dao.TvDao;
import com.olimpiait.agutierrezt.movie.database.model.genres.GenreMovie;
import com.olimpiait.agutierrezt.movie.database.model.genres.GenreResult;
import com.olimpiait.agutierrezt.movie.database.model.genres.GenreTv;
import com.olimpiait.agutierrezt.movie.database.model.movies.MovieResult;
import com.olimpiait.agutierrezt.movie.database.model.tv_shows.TvResult;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

@Database(entities = {MovieResult.class, GenreResult.class, TvResult.class, GenreTv.class, GenreMovie.class}, version = 1)
@TypeConverters({DateConverters.class})
public abstract class MovieDatabase extends RoomDatabase
{
    private static volatile MovieDatabase INSTANCE;

    //DAO
    public abstract MovieDao movieDao();
    public abstract TvDao tvDao();
    public abstract GenreDao genreDao();
    public abstract GenreMovieDao genreMovieDao();
    public abstract GenreTvDao genreTvDao();
}
