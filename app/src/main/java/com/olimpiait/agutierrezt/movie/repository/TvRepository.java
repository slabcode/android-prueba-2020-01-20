package com.olimpiait.agutierrezt.movie.repository;

import com.olimpiait.agutierrezt.movie.api.MovieApi;
import com.olimpiait.agutierrezt.movie.database.dao.GenreTvDao;
import com.olimpiait.agutierrezt.movie.database.dao.TvDao;
import com.olimpiait.agutierrezt.movie.database.model.genres.GenreTv;
import com.olimpiait.agutierrezt.movie.database.model.tv_shows.Tv;
import com.olimpiait.agutierrezt.movie.database.model.tv_shows.TvResult;
import com.olimpiait.agutierrezt.movie.database.model.tv_shows.TvWithGenre;

import java.util.List;
import java.util.concurrent.Executor;

import javax.inject.Inject;

import androidx.lifecycle.LiveData;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TvRepository {
    private final MovieApi api;
    private final TvDao tvDao;
    private final GenreTvDao genreTvDao;
    private final Executor executor;
    private final String API_KEY = "078b798e54c62cb1ae892fe16d7c5bba";

    @Inject
    public TvRepository(MovieApi api, TvDao tvDao, GenreTvDao genreTvDao, Executor executor) {
        this.api = api;
        this.tvDao = tvDao;
        this.executor = executor;
        this.genreTvDao = genreTvDao;
    }

    public LiveData<List<TvResult>> getAllTvShows(int page) {
        loadTvShows(page);
        return tvDao.getAllTvShows(page);
    }

    public LiveData<TvWithGenre> getTvShowById(int id) {
        return tvDao.getTvShowById(id);
    }

    private void loadTvShows(int page) {
        executor.execute(() -> {
            boolean isEmpty = tvDao.hasTvShows(page) == null;
            if(isEmpty) {
                api.GetTvTopRated(page, API_KEY).enqueue(new Callback<Tv>() {
                    @Override
                    public void onResponse(Call<Tv> call, Response<Tv> response) {
                        executor.execute(() -> {
                            Tv tv = response.body();
                            for (TvResult result: tv.getResults()) {
                                result.setPage(tv.getPage());
                                result.setTotalPages(tv.getTotalPages());
                                tvDao.save(result);
                                for(int genre: result.getGenres()) {
                                    genreTvDao.save(new GenreTv(result.getId(), genre));
                                }
                            }
                        });
                    }

                    @Override
                    public void onFailure(Call<Tv> call, Throwable t) {

                    }
                });
            }
        });
    }
}
