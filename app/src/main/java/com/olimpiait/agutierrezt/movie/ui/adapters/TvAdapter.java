package com.olimpiait.agutierrezt.movie.ui.adapters;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.olimpiait.agutierrezt.movie.R;
import com.olimpiait.agutierrezt.movie.database.model.genres.GenreResult;
import com.olimpiait.agutierrezt.movie.database.model.movies.MovieResult;
import com.olimpiait.agutierrezt.movie.database.model.tv_shows.Tv;
import com.olimpiait.agutierrezt.movie.database.model.tv_shows.TvResult;
import com.olimpiait.agutierrezt.movie.databinding.GenreBinding;
import com.olimpiait.agutierrezt.movie.databinding.MovieBinding;
import com.olimpiait.agutierrezt.movie.databinding.TvBinding;
import com.olimpiait.agutierrezt.movie.interfaces.MovieClickListener;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

public class TvAdapter extends RecyclerView.Adapter<TvAdapter.ViewHolder> {
    private MovieClickListener movieClickListener;
    private List<TvResult> tvs;
    private Context context;
    private LayoutInflater layoutInflater;

    public TvAdapter(List<TvResult> tvs, Context context) {
        this.tvs = tvs;
        this.context = context;
    }

    public void setMovieClickListener(MovieClickListener movieClickListener) {
        this.movieClickListener = movieClickListener;
    }

    @Override
    public void onBindViewHolder(@NonNull TvAdapter.ViewHolder holder, int position) {
        TvResult tvResult = tvs.get(position);
        holder.tvBinding.getRoot().findViewById(R.id.tv_recycler_card).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                movieClickListener.setOnClickListener(tvResult.getId());
            }
        });
        holder.bind(tvResult);
    }

    @NonNull
    @Override
    public TvAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (layoutInflater==null){
            layoutInflater=LayoutInflater.from(parent.getContext());
        }
        TvBinding tvBinding= DataBindingUtil.inflate(layoutInflater, R.layout.tv_item,parent,false);
        return new TvAdapter.ViewHolder(tvBinding);
    }

    @Override
    public int getItemCount() {
        return tvs.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        private TvBinding tvBinding;
        public ViewHolder(@NonNull TvBinding tvBinding) {
            super(tvBinding.getRoot());
            this.tvBinding=tvBinding;
        }
        public void bind(TvResult tvResult){
            this.tvBinding.setTv(tvResult);
            tvBinding.executePendingBindings();
        }
        public TvBinding getTvBinding(){
            return tvBinding;
        }
    }
}
