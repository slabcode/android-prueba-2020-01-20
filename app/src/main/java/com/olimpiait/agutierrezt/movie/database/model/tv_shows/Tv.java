package com.olimpiait.agutierrezt.movie.database.model.tv_shows;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Tv {
    @SerializedName("page")
    @Expose
    private int page;
    @SerializedName("total_pages")
    @Expose
    private int totalPages;
    @SerializedName("results")
    private List<TvResult> results;

    public Tv(int page, int totalPages, List<TvResult> results) {
        this.page = page;
        this.totalPages = totalPages;
        this.results = results;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public List<TvResult> getResults() {
        return results;
    }

    public void setResults(List<TvResult> results) {
        this.results = results;
    }

}
