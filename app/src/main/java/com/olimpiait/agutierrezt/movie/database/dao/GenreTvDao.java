package com.olimpiait.agutierrezt.movie.database.dao;

import com.olimpiait.agutierrezt.movie.database.model.genres.GenreTv;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

@Dao
public interface GenreTvDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void save(GenreTv genreTv);

    @Query("SELECT * FROM genre_tvs WHERE tvId = :tvId AND genreId = :genreId  LIMIT 1")
    LiveData<GenreTv> hasGenreTv(int tvId, int genreId);
}
