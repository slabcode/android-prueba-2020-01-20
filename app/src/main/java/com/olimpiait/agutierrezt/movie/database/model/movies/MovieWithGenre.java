package com.olimpiait.agutierrezt.movie.database.model.movies;

import android.view.View;

import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;
import com.olimpiait.agutierrezt.movie.database.model.genres.GenreMovie;
import com.olimpiait.agutierrezt.movie.database.model.genres.Genre;
import com.olimpiait.agutierrezt.movie.database.model.genres.GenreResult;

import java.util.List;

import androidx.databinding.BindingAdapter;
import androidx.room.Embedded;
import androidx.room.Junction;
import androidx.room.Relation;

public class MovieWithGenre {
    @Embedded
    private MovieResult movie;
    @Relation(
            parentColumn = "movieId",
            entityColumn = "genreId",
            associateBy = @Junction(GenreMovie.class)
    )
    private List<GenreResult> genres;

    public MovieWithGenre(MovieResult movie, List<GenreResult> genres) {
        this.movie = movie;
        this.genres = genres;
    }

    public MovieResult getMovie() {
        return movie;
    }

    public void setMovie(MovieResult movie) {
        this.movie = movie;
    }

    public List<GenreResult> getGenres() {
        return genres;
    }

    public void setGenres(List<GenreResult> genres) {
        this.genres = genres;
    }

    @BindingAdapter({"getGenres"})
    public static void getNames(ChipGroup chipGroup, List<GenreResult> genres) {
        String text = "";
        if(genres != null) {
            for(GenreResult genre: genres) {
                text += genre.getName();
                Chip chip = new Chip(chipGroup.getContext());
                chip.setText(genre.getName());
                chipGroup.addView(chip);
            }
        }
        if(text.isEmpty()) {
            chipGroup.setVisibility(View.GONE);
        }
    }


}
