package com.olimpiait.agutierrezt.movie.database.model.tv_shows;

import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.olimpiait.agutierrezt.movie.database.converter.DateConverters;

import java.util.Date;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.BindingAdapter;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "tv_shows")
public class TvResult {
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "tvId")
    @SerializedName("id")
    @Expose
    private int id;
    @NonNull
    @ColumnInfo(name = "votes")
    @SerializedName("vote_count")
    @Expose
    private int votes;
    @NonNull
    @ColumnInfo(name = "poster")
    @SerializedName("poster_path")
    @Expose
    private String poster;
    @NonNull
    @ColumnInfo(name = "name")
    @SerializedName("name")
    @Expose
    private String name;
    @NonNull
    @ColumnInfo(name = "vote_average")
    @SerializedName("vote_average")
    @Expose
    private float voteAverage;
    @NonNull
    @ColumnInfo(name = "overview")
    @SerializedName("overview")
    @Expose
    private String overview;
    @NonNull
    @ColumnInfo(name = "first_air_date")
    @SerializedName("first_air_date")
    @Expose
    private Date release;
    @Ignore
    @SerializedName("genre_ids")
    @Expose
    private List<Integer> genres;
    @NonNull
    @ColumnInfo(name = "page")
    private int page;
    @NonNull
    @ColumnInfo(name = "total_pages")
    private int totalPages;

    public TvResult() {
    }

    public TvResult(int id, int votes, @NonNull String poster, @NonNull String name, float voteAverage, @NonNull String overview, @NonNull Date release, List<Integer> genres) {
        this.id = id;
        this.votes = votes;
        this.poster = poster;
        this.name = name;
        this.voteAverage = voteAverage;
        this.overview = overview;
        this.release = release;
        this.genres = genres;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getVotes() {
        return votes;
    }

    public void setVotes(int votes) {
        this.votes = votes;
    }

    @NonNull
    public String getPoster() {
        return poster;
    }

    public void setPoster(@NonNull String poster) {
        this.poster = poster;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    public float getVoteAverage() {
        return voteAverage;
    }

    public void setVoteAverage(float voteAverage) {
        this.voteAverage = voteAverage;
    }

    @NonNull
    public String getOverview() {
        return overview;
    }

    public void setOverview(@NonNull String overview) {
        this.overview = overview;
    }

    @NonNull
    public Date getRelease() {
        return release;
    }

    public void setRelease(@NonNull Date release) {
        this.release = release;
    }

    public List<Integer> getGenres() {
        return genres;
    }

    public void setGenres(List<Integer> genres) {
        this.genres = genres;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    @BindingAdapter({"imageUrl"})
    public static void loadimage(ImageView imageView, String imageUrl){
        Glide.with(imageView.getContext()).load("https://image.tmdb.org/t/p/w500" + imageUrl).into(imageView);
    }

    public String shorterText() {
        if(overview.length() < 50)
            return overview;
        return overview.substring(0,50) + "...";
    }

    public String voteAverageText() {
        return voteAverage + "";
    }

    public String dateStr() {
        return DateConverters.dateToString(release);
    }
}
