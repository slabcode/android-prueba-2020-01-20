package com.olimpiait.agutierrezt.movie.di.component;


import android.app.Application;

import com.olimpiait.agutierrezt.movie.App;
import com.olimpiait.agutierrezt.movie.di.module.ActivityModule;
import com.olimpiait.agutierrezt.movie.di.module.DatabaseModule;
import com.olimpiait.agutierrezt.movie.di.module.FragmentModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;
import dagger.android.AndroidInjector;


@Singleton
@Component(modules = {AndroidInjectionModule.class, DatabaseModule.class, ActivityModule.class, FragmentModule.class})
public  interface AppComponent extends AndroidInjector<App> {
}
