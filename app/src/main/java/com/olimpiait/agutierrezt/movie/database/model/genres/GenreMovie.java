package com.olimpiait.agutierrezt.movie.database.model.genres;

import androidx.annotation.NonNull;
import androidx.room.Entity;

@Entity(primaryKeys = {"movieId", "genreId"}, tableName = "genre_movies")
public class GenreMovie {
    private int movieId;
    private int genreId;

    public GenreMovie(@NonNull int movieId, @NonNull int genreId) {
        this.movieId = movieId;
        this.genreId = genreId;
    }

    public int getMovieId() {
        return movieId;
    }

    public void setMovieId(int movieId) {
        this.movieId = movieId;
    }

    public int getGenreId() {
        return genreId;
    }

    public void setGenreId(int genreId) {
        this.genreId = genreId;
    }
}
