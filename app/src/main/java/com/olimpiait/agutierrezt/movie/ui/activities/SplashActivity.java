package com.olimpiait.agutierrezt.movie.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import dagger.android.AndroidInjection;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.olimpiait.agutierrezt.movie.R;
import com.olimpiait.agutierrezt.movie.viewmodel.FactoryViewModel;
import com.olimpiait.agutierrezt.movie.viewmodel.GenreViewModel;

import javax.inject.Inject;

public class SplashActivity extends AppCompatActivity {

    @Inject
    FactoryViewModel factoryViewModel;
    private GenreViewModel genreViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        configureDagger();
        genreViewModel = ViewModelProviders.of(this, factoryViewModel).get(GenreViewModel.class);
        genreViewModel.getAllGenres().observe(this,genreResults -> {
            if(genreResults.size() > 0) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
                        startActivity(intent);
                        finish();
                    }
                },3000);

            }
        });

    }

    private void configureDagger(){
        AndroidInjection.inject(this);
    }
}
