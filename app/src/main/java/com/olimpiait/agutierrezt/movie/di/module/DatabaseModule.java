package com.olimpiait.agutierrezt.movie.di.module;

import android.content.Context;

import com.olimpiait.agutierrezt.movie.api.MovieApi;
import com.olimpiait.agutierrezt.movie.database.MovieDatabase;
import com.olimpiait.agutierrezt.movie.database.dao.GenreDao;
import com.olimpiait.agutierrezt.movie.database.dao.GenreMovieDao;
import com.olimpiait.agutierrezt.movie.database.dao.GenreTvDao;
import com.olimpiait.agutierrezt.movie.database.dao.MovieDao;
import com.olimpiait.agutierrezt.movie.database.dao.TvDao;
import com.olimpiait.agutierrezt.movie.repository.GenreRepository;
import com.olimpiait.agutierrezt.movie.repository.MovieRepository;
import com.olimpiait.agutierrezt.movie.repository.TvRepository;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import javax.inject.Inject;
import javax.inject.Singleton;

import androidx.room.Room;
import dagger.Module;
import dagger.Provides;

@Module(includes = {ApiModule.class,ViewModelModule.class,ContextModule.class})
public class DatabaseModule {

    //DATABASE INJECTION

    @Provides
    @Singleton
    @Inject
    public MovieDatabase provideDatabase(Context context) {
        return Room.databaseBuilder(context, MovieDatabase.class, "MovieDatabase.db").build();
    }

    @Provides
    @Singleton
    public MovieDao movieDao(MovieDatabase movieDatabase) {
        return movieDatabase.movieDao();
    }

    @Provides
    @Singleton
    public GenreDao genreDao(MovieDatabase movieDatabase) {
        return movieDatabase.genreDao();
    }

    @Provides
    @Singleton
    public TvDao tvDao(MovieDatabase movieDatabase) {
        return movieDatabase.tvDao();
    }

    @Provides
    @Singleton
    public GenreMovieDao genreMovieDao(MovieDatabase movieDatabase) {
        return movieDatabase.genreMovieDao();
    }

    @Provides
    @Singleton
    public GenreTvDao genreTvDao(MovieDatabase movieDatabase) {
        return movieDatabase.genreTvDao();
    }

    //REPOSITORY INJECTION

    @Provides
    Executor provideExecutor() {
        return Executors.newSingleThreadExecutor();
    }

    @Provides
    @Singleton
    public GenreRepository genreRepository(MovieApi movieApi, GenreDao genreDao, Executor executor){
        return new GenreRepository(movieApi,genreDao,executor);
    }

    @Provides
    @Singleton
    public MovieRepository movieRepository(MovieApi api, MovieDao movieDao, GenreMovieDao genreMovieDao, Executor executor) {
        return new MovieRepository(api,movieDao,genreMovieDao,executor);
    }

    @Provides
    @Singleton
    public TvRepository tvRepository(MovieApi api, TvDao tvDao, GenreTvDao genreTvDao, Executor executor) {
        return new TvRepository(api,tvDao,genreTvDao,executor);
    }


}
