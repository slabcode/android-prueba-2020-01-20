package com.olimpiait.agutierrezt.movie.database.model.tv_shows;

import android.view.View;

import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;
import com.olimpiait.agutierrezt.movie.database.model.genres.Genre;
import com.olimpiait.agutierrezt.movie.database.model.genres.GenreResult;
import com.olimpiait.agutierrezt.movie.database.model.genres.GenreTv;

import java.util.List;

import androidx.databinding.BindingAdapter;
import androidx.room.Embedded;
import androidx.room.Junction;
import androidx.room.Relation;

public class TvWithGenre {
    @Embedded
    private TvResult tv;
    @Relation(
            parentColumn = "tvId",
            entityColumn = "genreId",
            associateBy = @Junction(GenreTv.class)
    )
    private List<GenreResult> genres;

    public TvWithGenre(TvResult tv, List<GenreResult> genres) {
        this.tv = tv;
        this.genres = genres;
    }

    public TvResult getTv() {
        return tv;
    }

    public void setTv(TvResult tv) {
        this.tv = tv;
    }

    public List<GenreResult> getGenres() {
        return genres;
    }

    public void setGenres(List<GenreResult> genres) {
        this.genres = genres;
    }

    @BindingAdapter({"getGenres"})
    public static void getNames(ChipGroup chipGroup, List<GenreResult> genres) {
        String text = "";
        if(genres != null) {
            for(GenreResult genre: genres) {
                text += genre.getName();
                Chip chip = new Chip(chipGroup.getContext());
                chip.setText(genre.getName());
                chipGroup.addView(chip);
            }
        }
        if(text.isEmpty()) {
            chipGroup.setVisibility(View.GONE);
        }
    }
}
