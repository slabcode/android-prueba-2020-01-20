package com.olimpiait.agutierrezt.movie.ui.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.olimpiait.agutierrezt.movie.R;
import com.olimpiait.agutierrezt.movie.database.model.tv_shows.TvResult;
import com.olimpiait.agutierrezt.movie.interfaces.ChangeActivityListener;
import com.olimpiait.agutierrezt.movie.interfaces.MovieClickListener;
import com.olimpiait.agutierrezt.movie.ui.adapters.TvAdapter;
import com.olimpiait.agutierrezt.movie.util.Constants;
import com.olimpiait.agutierrezt.movie.util.EndlessRecyclerViewScrollListener;
import com.olimpiait.agutierrezt.movie.viewmodel.FactoryViewModel;
import com.olimpiait.agutierrezt.movie.viewmodel.TvShowViewModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import dagger.android.support.AndroidSupportInjection;

public class TvFragment extends Fragment {
    private RecyclerView recyclerView;
    private TvAdapter tvAdapter;
    private LinearLayoutManager mLayoutManager;
    @Inject
    FactoryViewModel factoryViewModel;
    private TvShowViewModel tvShowViewModel;
    private boolean hasFinish = false;
    private List<TvResult> tvs;
    private ChangeActivityListener changeActivityListener;

    public void setChangeActivityListener(ChangeActivityListener changeActivityListener) {
        this.changeActivityListener = changeActivityListener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tv, container, false);
        configureDagger();
        init(view);
        events();
        return view;
    }

    private void init(View view) {
        tvs = new ArrayList<>();
        recyclerView = view.findViewById(R.id.tv_recycler);
        mLayoutManager = new LinearLayoutManager(TvFragment.this.getActivity());
        tvAdapter = new TvAdapter(tvs,TvFragment.this.getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL));
        recyclerView.setAdapter(tvAdapter);
        tvShowViewModel = ViewModelProviders.of(this, factoryViewModel).get(TvShowViewModel.class);
        loadTvShows(1);

    }

    private void events() {
        recyclerView.setOnScrollListener(new EndlessRecyclerViewScrollListener(mLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if(!hasFinish) {
                    loadTvShows(page + 1);
                }
            }
        });
        tvAdapter.setMovieClickListener(new MovieClickListener() {
            @Override
            public void setOnClickListener(int id) {
                changeActivityListener.setOnClickListener(id, Constants.TV);
            }
        });
    }

    private void loadTvShows(int page) {
        tvShowViewModel.getTvShows(page).observe(this,tvResults -> {
            if(tvResults.size() > 0) {
                tvs.addAll(tvResults);
                tvAdapter.notifyDataSetChanged();
            }else {
                hasFinish = true;
            }
        });
    }

    private void configureDagger(){
        AndroidSupportInjection.inject(this);
    }
}
