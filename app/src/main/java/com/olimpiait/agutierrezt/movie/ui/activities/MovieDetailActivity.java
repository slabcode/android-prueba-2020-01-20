package com.olimpiait.agutierrezt.movie.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import dagger.android.AndroidInjection;

import android.os.Bundle;

import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.olimpiait.agutierrezt.movie.R;
import com.olimpiait.agutierrezt.movie.databinding.MovieWithGenreBinding;
import com.olimpiait.agutierrezt.movie.util.Constants;
import com.olimpiait.agutierrezt.movie.viewmodel.FactoryViewModel;
import com.olimpiait.agutierrezt.movie.viewmodel.MovieViewModel;

import javax.inject.Inject;

public class MovieDetailActivity extends AppCompatActivity {

    private MovieWithGenreBinding movieWithGenreBinding;
    @Inject
    FactoryViewModel factoryViewModel;
    private MovieViewModel movieViewModel;
    private int id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        configureDagger();
        movieWithGenreBinding = DataBindingUtil.setContentView(this,R.layout.activity_movie_detail);
        id = getIntent().getIntExtra(Constants.ID,0);
        init();
    }

    private void init() {
        movieViewModel = ViewModelProviders.of(this, factoryViewModel).get(MovieViewModel.class);
        movieViewModel.getMovieById(id).observe(this, movieWithGenre -> {
            movieWithGenreBinding.setMovieWithGenre(movieWithGenre);
        });

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        CollapsingToolbarLayout collapsingToolbarLayout =
                (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);

    }

    private void configureDagger(){
        AndroidInjection.inject(this);
    }

}
