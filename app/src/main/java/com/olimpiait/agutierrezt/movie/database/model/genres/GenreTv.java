package com.olimpiait.agutierrezt.movie.database.model.genres;

import androidx.room.Entity;

@Entity(primaryKeys = {"tvId", "genreId"}, tableName = "genre_tvs")
public class GenreTv {
    private int tvId;
    private int genreId;

    public GenreTv(int tvId, int genreId) {
        this.tvId = tvId;
        this.genreId = genreId;
    }

    public int getTvId() {
        return tvId;
    }

    public void setTvId(int tvId) {
        this.tvId = tvId;
    }

    public int getGenreId() {
        return genreId;
    }

    public void setGenreId(int genreId) {
        this.genreId = genreId;
    }
}
