package com.olimpiait.agutierrezt.movie.di.module;

import com.olimpiait.agutierrezt.movie.ui.activities.HomeActivity;
import com.olimpiait.agutierrezt.movie.ui.activities.MovieDetailActivity;
import com.olimpiait.agutierrezt.movie.ui.activities.SplashActivity;
import com.olimpiait.agutierrezt.movie.ui.activities.TvDetailActivity;


import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityModule {
    @ContributesAndroidInjector()
    abstract SplashActivity contributeSplashActivity();
    @ContributesAndroidInjector(modules = {FragmentModule.class})
    abstract HomeActivity contributeHomeActivity();
    @ContributesAndroidInjector()
    abstract MovieDetailActivity contributeMovieDetailActivity();
    @ContributesAndroidInjector()
    abstract TvDetailActivity contributeTvDetailActivity();


}
