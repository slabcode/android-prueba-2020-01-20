package com.olimpiait.agutierrezt.movie.ui.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import dagger.android.AndroidInjection;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.olimpiait.agutierrezt.movie.R;
import com.olimpiait.agutierrezt.movie.interfaces.ChangeActivityListener;
import com.olimpiait.agutierrezt.movie.ui.fragments.GenreFragment;
import com.olimpiait.agutierrezt.movie.ui.fragments.MovieFragment;
import com.olimpiait.agutierrezt.movie.ui.fragments.TvFragment;
import com.olimpiait.agutierrezt.movie.util.Constants;

import javax.inject.Inject;

public class HomeActivity extends AppCompatActivity implements HasSupportFragmentInjector, ChangeActivityListener {

    private BottomNavigationView mBottomNavigation;
    private Toolbar mToolbar;


    @Inject
    DispatchingAndroidInjector<Fragment> dispatchingAndroidInjector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        configureDagger();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        init();
        events();


    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return dispatchingAndroidInjector;
    }

    private void init() {
        mBottomNavigation = findViewById(R.id.home_bottom_navigation);
        mToolbar = findViewById(R.id.movie_toolbar);
        setSupportActionBar(mToolbar);
        MovieFragment movieFragment = new MovieFragment();
        movieFragment.setChangeActivityListener(this);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.home_fragment,movieFragment,Constants.MOVIE);
        transaction.addToBackStack(null);

        transaction.commit();
    }

    private void events() {
        mBottomNavigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.bottom_movie:
                        MovieFragment movie = (MovieFragment) getSupportFragmentManager().findFragmentByTag(Constants.MOVIE);
                        if (movie == null || !movie.isVisible()) {
                            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                            MovieFragment movieFragment = new MovieFragment();
                            movieFragment.setChangeActivityListener(HomeActivity.this);
                            transaction.replace(R.id.home_fragment,movieFragment,Constants.MOVIE);
                            transaction.addToBackStack(null);

                            transaction.commit();
                        }
                        return true;
                    case R.id.bottom_tv:
                        TvFragment tv = (TvFragment) getSupportFragmentManager().findFragmentByTag(Constants.TV);
                        if (tv == null || !tv.isVisible()) {
                            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                            TvFragment tvFragment = new TvFragment();
                            tvFragment.setChangeActivityListener(HomeActivity.this);
                            transaction.replace(R.id.home_fragment,tvFragment,Constants.TV);
                            transaction.addToBackStack(null);
                            transaction.commit();
                        }
                        return true;

                    case R.id.bottom_genre:
                        GenreFragment genre = (GenreFragment) getSupportFragmentManager().findFragmentByTag(Constants.GENRE);
                        if (genre == null || !genre.isVisible()) {
                            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                            transaction.replace(R.id.home_fragment,new GenreFragment(),Constants.GENRE);
                            transaction.addToBackStack(null);

                            transaction.commit();
                        }
                        return true;
                }
                return false;
            }
        });
    }

    private void configureDagger(){
        AndroidInjection.inject(this);
    }

    @Override
    public void setOnClickListener(int id, String type) {
        if(type.equals(Constants.MOVIE)) {
            Intent intent =  new Intent(this,MovieDetailActivity.class);
            intent.putExtra(Constants.ID,id);
            startActivity(intent);
        }else {
            Intent intent =  new Intent(this,TvDetailActivity.class);
            intent.putExtra(Constants.ID,id);
            startActivity(intent);
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
