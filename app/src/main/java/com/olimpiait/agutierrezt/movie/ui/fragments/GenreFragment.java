package com.olimpiait.agutierrezt.movie.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.olimpiait.agutierrezt.movie.R;
import com.olimpiait.agutierrezt.movie.ui.activities.HomeActivity;
import com.olimpiait.agutierrezt.movie.ui.activities.SplashActivity;
import com.olimpiait.agutierrezt.movie.ui.adapters.GenreAdapter;
import com.olimpiait.agutierrezt.movie.viewmodel.FactoryViewModel;
import com.olimpiait.agutierrezt.movie.viewmodel.GenreViewModel;

import javax.inject.Inject;


import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import dagger.android.support.AndroidSupportInjection;

public class GenreFragment extends Fragment {
    private RecyclerView recyclerView;
    private GenreAdapter genreAdapter;
    @Inject
    FactoryViewModel factoryViewModel;
    private GenreViewModel genreViewModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_genre, container, false);
        configureDagger();
        init(view);
        return view;
    }

    private void init(View view) {
        recyclerView = view.findViewById(R.id.genre_recycler);
        genreViewModel = ViewModelProviders.of(this, factoryViewModel).get(GenreViewModel.class);
        genreViewModel.getAllGenres().observe(this,genreResults -> {
            if(genreResults.size() > 0) {
                genreAdapter = new GenreAdapter(genreResults,GenreFragment.this.getActivity());
                recyclerView.setLayoutManager(new LinearLayoutManager(GenreFragment.this.getActivity()));
                recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL));
                recyclerView.setAdapter(genreAdapter);

            }
        });

    }

    private void configureDagger(){
        AndroidSupportInjection.inject(this);
    }
}
