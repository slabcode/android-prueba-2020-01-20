package com.olimpiait.agutierrezt.movie.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.olimpiait.agutierrezt.movie.R;
import com.olimpiait.agutierrezt.movie.database.model.genres.GenreResult;
import com.olimpiait.agutierrezt.movie.databinding.GenreBinding;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

public class GenreAdapter extends RecyclerView.Adapter<GenreAdapter.ViewHolder> {

    private List<GenreResult> genres;
    private Context context;
    private LayoutInflater layoutInflater;

    public GenreAdapter(List<GenreResult> genres, Context context) {
        this.genres = genres;
        this.context = context;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        GenreResult genreResult = genres.get(position);
        holder.bind(genreResult);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (layoutInflater==null){
            layoutInflater=LayoutInflater.from(parent.getContext());
        }
        GenreBinding genreBinding= DataBindingUtil.inflate(layoutInflater, R.layout.genre_item,parent,false);
        return new ViewHolder(genreBinding);
    }

    @Override
    public int getItemCount() {
        return genres.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        private GenreBinding genreBinding;
        public ViewHolder(@NonNull GenreBinding genreBinding) {
            super(genreBinding.getRoot());
            this.genreBinding=genreBinding;
        }
        public void bind(GenreResult genreResult){
            this.genreBinding.setGenre(genreResult);
            genreBinding.executePendingBindings();
        }
        public GenreBinding getGenreBinding(){
            return genreBinding;
        }
    }


}
