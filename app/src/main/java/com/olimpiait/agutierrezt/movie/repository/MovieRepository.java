package com.olimpiait.agutierrezt.movie.repository;

import com.olimpiait.agutierrezt.movie.api.MovieApi;
import com.olimpiait.agutierrezt.movie.database.dao.GenreMovieDao;
import com.olimpiait.agutierrezt.movie.database.dao.GenreTvDao;
import com.olimpiait.agutierrezt.movie.database.dao.MovieDao;
import com.olimpiait.agutierrezt.movie.database.dao.TvDao;
import com.olimpiait.agutierrezt.movie.database.model.genres.GenreMovie;
import com.olimpiait.agutierrezt.movie.database.model.genres.GenreTv;
import com.olimpiait.agutierrezt.movie.database.model.movies.Movie;
import com.olimpiait.agutierrezt.movie.database.model.movies.MovieResult;
import com.olimpiait.agutierrezt.movie.database.model.movies.MovieWithGenre;
import com.olimpiait.agutierrezt.movie.database.model.tv_shows.Tv;
import com.olimpiait.agutierrezt.movie.database.model.tv_shows.TvResult;
import com.olimpiait.agutierrezt.movie.database.model.tv_shows.TvWithGenre;

import java.util.List;
import java.util.concurrent.Executor;

import javax.inject.Inject;

import androidx.lifecycle.LiveData;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MovieRepository {
    private final MovieApi api;
    private final MovieDao movieDao;
    private final GenreMovieDao genreMovieDao;
    private final Executor executor;
    private final String API_KEY = "078b798e54c62cb1ae892fe16d7c5bba";

    @Inject
    public MovieRepository(MovieApi api, MovieDao movieDao, GenreMovieDao genreMovieDao, Executor executor) {
        this.api = api;
        this.movieDao = movieDao;
        this.genreMovieDao = genreMovieDao;
        this.executor = executor;
    }

    public LiveData<List<MovieResult>> getAllMovies(int page) {
        loadTvShows(page);
        return movieDao.getAllMovies(page);
    }

    public LiveData<MovieWithGenre> getMovieById(int id) {
        return movieDao.getMovie(id);
    }

    private void loadTvShows(int page) {
        executor.execute(() -> {
            boolean isEmpty = movieDao.hasMovies(page) == null;
            if(isEmpty) {
                api.GetMovieTopRated(page, API_KEY).enqueue(new Callback<Movie>() {
                    @Override
                    public void onResponse(Call<Movie> call, Response<Movie> response) {
                        executor.execute(() -> {
                            Movie movie = response.body();
                            for (MovieResult result: movie.getResults()) {
                                result.setPage(movie.getPage());
                                result.setTotalPages(movie.getTotalPages());
                                movieDao.save(result);
                                for(int genre: result.getGenres()) {
                                    genreMovieDao.save(new GenreMovie(result.getId(), genre));
                                }
                            }
                        });
                    }

                    @Override
                    public void onFailure(Call<Movie> call, Throwable t) {

                    }
                });
            }
        });
    }
}
