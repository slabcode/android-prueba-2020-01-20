package com.olimpiait.agutierrezt.movie.repository;

import com.olimpiait.agutierrezt.movie.api.MovieApi;
import com.olimpiait.agutierrezt.movie.database.dao.GenreDao;
import com.olimpiait.agutierrezt.movie.database.model.genres.Genre;
import com.olimpiait.agutierrezt.movie.database.model.genres.GenreResult;

import java.util.List;
import java.util.concurrent.Executor;

import javax.inject.Inject;

import androidx.lifecycle.LiveData;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GenreRepository {
    private final MovieApi api;
    private final GenreDao genreDao;
    private final Executor executor;
    private final String API_KEY = "078b798e54c62cb1ae892fe16d7c5bba";

    @Inject
    public GenreRepository(MovieApi api, GenreDao genreDao, Executor executor) {
        this.api = api;
        this.genreDao = genreDao;
        this.executor = executor;
    }

    public LiveData<List<GenreResult>> getAllGenres() {
        loadGenres();
        return genreDao.getAllGenres();
    }

    private void loadGenres() {
        executor.execute(() -> {
            boolean isEmpty = genreDao.hasGenres() == null;
            if(isEmpty) {
                api.GetGenres(API_KEY).enqueue(new Callback<Genre>() {
                    @Override
                    public void onResponse(Call<Genre> call, Response<Genre> response) {
                        if(response.isSuccessful()) {
                            executor.execute(() -> {
                                Genre genre = response.body();
                                genreDao.save(genre.getGenres());

                            });

                        }
                    }

                    @Override
                    public void onFailure(Call<Genre> call, Throwable t) {

                    }
                });
            }
        });
    }
}
