package com.olimpiait.agutierrezt.movie.interfaces;

public interface MovieClickListener {
    void setOnClickListener(int id);
}
