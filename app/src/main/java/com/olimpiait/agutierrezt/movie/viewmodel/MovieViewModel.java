package com.olimpiait.agutierrezt.movie.viewmodel;

import com.olimpiait.agutierrezt.movie.database.model.movies.MovieResult;
import com.olimpiait.agutierrezt.movie.database.model.movies.MovieWithGenre;
import com.olimpiait.agutierrezt.movie.repository.MovieRepository;

import java.util.List;

import javax.inject.Inject;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

public class MovieViewModel extends ViewModel {
    private MovieRepository movieRepository;

    @Inject
    public MovieViewModel(MovieRepository movieRepository) {
        this.movieRepository = movieRepository;
    }

    public LiveData<List<MovieResult>> getMovies(int page) {
        return movieRepository.getAllMovies(page);
    }

    public LiveData<MovieWithGenre> getMovieById(int id) {
        return movieRepository.getMovieById(id);
    }
}
