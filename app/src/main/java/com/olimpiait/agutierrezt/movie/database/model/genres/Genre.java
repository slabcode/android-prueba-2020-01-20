package com.olimpiait.agutierrezt.movie.database.model.genres;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class Genre {
    @SerializedName("genres")
    @Expose
    private List<GenreResult> genres;

    public Genre(List<GenreResult> genres) {
        this.genres = genres;
    }

    public List<GenreResult> getGenres() {
        return genres;
    }

    public void setGenres(List<GenreResult> genres) {
        this.genres = genres;
    }
}
